package INF101.lab1.INF100labs;

import javax.security.auth.kerberos.KerberosCredMessage;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        //multiplesOfSevenUpTo(49);
        
    }

    public static void multiplesOfSevenUpTo(int n) {
        for (int i = 0; i <= n; i+=1){
            if (i == 0) {continue;}
            else if (i % 7 == 0) {System.out.println(i);}}
    }



    public static void multiplicationTable(int n) {

        for (int i = 1; i <= n; i += 1) {
            if (i != 1) {
                System.out.println("");}
            System.out.print(i + ": ");

            for (int j = 1; j <= n; j += 1) {
                System.out.print(i*j + " ");
                
            }
            
        }   
    }

    public static int crossSum(int num) {
        int crossSum = 0;

        for (int i = num; i > 0; i = i/10 ) {
            crossSum += i % 10;
        }
        return crossSum;
    }

}