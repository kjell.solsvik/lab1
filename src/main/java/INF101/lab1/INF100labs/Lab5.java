package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        //ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 2, 3));
        //ArrayList<Integer> list2 = new ArrayList<>(Arrays.asList(1, 2, 3));
        //System.out.println(list1.size());
        //addList(list1, list2);
        //System.out.println(list1);

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        for (int i = 0; i< list.size(); i++) {
            if (list.get(i) == 3) { list.remove(i); i --;}

        }
        return list;
    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> returnList = new ArrayList<Integer>();
        
        for (int i = 0; i < list.size(); i ++) {
            if (returnList.contains(list.get(i)) == false) {returnList.add(list.get(i));}

        }
        return returnList;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            Integer newValue = a.get(i) + b.get(i);
            a.set(i, newValue);
        }
        
    }

}