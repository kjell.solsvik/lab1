package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("game", "carrot", "champion");

        
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
        Integer maxLength = 0;

        Integer ord1 = word1.length();
        Integer ord2 = word2.length();
        
        Integer ord3 = word3.length();

        if (ord1 > maxLength) {maxLength = ord1;}
        if (ord2 > maxLength) {maxLength = ord2;}
        if (ord3 > maxLength) {maxLength = ord3;}
        

        if (ord1 == maxLength) 
            System.out.println(word1);
        
        if (ord2 == maxLength) 
            System.out.println(word2);
        if (ord3 == maxLength) 
            System.out.println(word3);
        

    }

    public static boolean isLeapYear(int year) {
        if (year % 400 == 0 && year % 100 == 0 && year % 4 == 0) {return true;}
        else if (year % 100 != 0 && year % 4 == 0) {return true;}
        else return false;
        
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num <0) {return false;}
        if (num % 2 != 0) {return false;}
        else return true;
    }

}
