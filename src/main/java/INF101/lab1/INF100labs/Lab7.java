package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));
        System.out.println(sumOfRows(grid1, 1));
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        for (int i = 0; i < grid.size(); i ++ ) {
            if (i == row) {grid.remove(row);}

        }
    }

    public static Integer sumOfCols(ArrayList<ArrayList<Integer>> grid, int col) {
        int sumOfCol = 0;
        for (int i = 0; i < grid.size(); i ++ ) {
            sumOfCol += grid.get(i).get(col);
            //System.out.println(sumOfCol);
        }
        return sumOfCol;
    }

    public static boolean sameAsNextCol(ArrayList<ArrayList<Integer>> grid) {
        for (int i = 0; i < grid.size() -1; i ++ ) {
            if (sumOfCols(grid, i) != sumOfCols(grid, i+1)) {return false;}
        }
        return true;
    }

    public static Integer sumOfRows(ArrayList<ArrayList<Integer>> grid, int row) {
        int sumOfRows = 0;
        for (int i = 0; i < grid.get(row).size(); i ++ ) {
            sumOfRows += grid.get(row).get(i);
            System.out.println(sumOfRows);
            }
        return sumOfRows;

    }

    public static boolean sameAsNextrow(ArrayList<ArrayList<Integer>> grid) {
        for (int i = 0; i < grid.get(i).size() -1; i ++ ) {
            if (sumOfRows(grid, i) != sumOfRows(grid, i+1)) {return false;}
        }
        return true;
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (sameAsNextrow(grid) == true && sameAsNextCol(grid) == true) {return true;}

        
        return false;
    }
}

